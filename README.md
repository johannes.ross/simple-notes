# Simple Notes

With this application, you will be abled to create simple notes.

# Planing

(3 min)

1. Implement only Frontend (React)
1. Include toastify
1. Create structure (Mobile First approach in mind)
1. Create functionality
1. Add CSS / SCSS for UX / UI
1. Optimise UX / UI

# Implement only Frontend (React)

(7 min)

# Include toastify

(3 min)

# Create structure (Mobile First approach in mind)

(20 min)

# Create functionality

(90 min)

# Notes

-   Adding Notes doesn't work because listening to the localstorage is not possible. Maybe use MobX instead!
-   UI / UX is not done yet
-   add and update shouldn't have the same logic. There should be only one update function which creates a new note, when no note is found
-   maybe remove the add function completly?

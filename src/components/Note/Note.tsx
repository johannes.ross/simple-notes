import { useState } from 'react';
import Store from '../../services/store';
import { NoteContent } from '../App';
import { newNoteText } from '../NewNote';
import './Note.css';

export type NoteState = 'NewOrEdit' | 'ReadOnly';

type NoteProps = NoteContent & {};
function Note(props: NoteProps) {
	const [noteState, setNoteState] = useState<NoteState>(
		newNoteText === props.text ? 'NewOrEdit' : 'ReadOnly'
	);
	const [noteContent, setNoteContent] = useState<string>(props.text);
	const handleNoteClick = () => {
		if ('ReadOnly' === noteState) {
			setNoteState('NewOrEdit');
		}
	};
	const handleSaveClick = () => {
		new Store().updateNoteInStore({ id: props.id, text: noteContent });
		setNoteState('ReadOnly');
	};
	const handleDelteClick = () => {
		new Store().removeNoteFromStore(props.id);
	};
	return (
		<article onClick={handleNoteClick}>
			<textarea
				readOnly={'ReadOnly' === noteState}
				value={noteContent}
				onChange={(e) => setNoteContent(e.target.value)}
			/>
			{'NewOrEdit' === noteState && (
				<button onClick={handleSaveClick}>Save</button>
			)}
			<button onClick={handleDelteClick}>Delete</button>
		</article>
	);
}

export default Note;

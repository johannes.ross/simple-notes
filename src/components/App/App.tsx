import './App.css';
import Header from '../Header';
import { useEffect, useState } from 'react';
import Store from '../../services/store';
import Note from '../Note';
import NewNote from '../NewNote';
import { toast } from 'react-toastify';

export type NoteContent = {
	id: number;
	text: string;
};

function App() {
	const [notes, setNotes] = useState<NoteContent[]>([]);
	const storedNotes = notes.map((savedNote) => {
		return (
			<Note key={savedNote.id} id={savedNote.id} text={savedNote.text} />
		);
	});
	const updateLocalNotes = () => {
		console.log('trigger Event');
		try {
			setNotes(new Store().getStoredNotes());
		} catch (error) {
			toast('You have no notes! Start adding notes now!', {
				type: 'success',
			});
		}
	};
	useEffect(() => {
		updateLocalNotes();
		window.addEventListener('storage', updateLocalNotes);
		return window.removeEventListener('storage', updateLocalNotes);
	}, []);

	return (
		<div className="App">
			<Header />
			<main>{storedNotes}</main>
			<NewNote />
		</div>
	);
}

export default App;

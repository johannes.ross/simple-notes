import './NewNote.css';
import { useState } from 'react';
import Note from '../Note';
export const newNoteText = 'Enter new Note here';

function NewNote() {
	const [addNote, setAddNote] = useState<boolean>(false);
	return (
		<>
			{addNote ? (
				<div className="NewNote">
					<Note id={Math.random()} text={newNoteText} />
					<button
						onClick={() => {
							setAddNote(false);
						}}
					>
						Done
					</button>
				</div>
			) : (
				<button onClick={() => setAddNote(true)}>Add new Note</button>
			)}
		</>
	);
}

export default NewNote;

import { toast } from 'react-toastify';
import { NoteContent } from '../components/App';

export default class Store {
	private noteStorage = 'simple-notes';
	private getRandomIdForNote = (notes: NoteContent[], id: number) => {
		let newId = id;
		while (-1 !== notes.findIndex((note) => newId === note.id)) {
			newId = Math.random();
		}
		return newId;
	};
	private saveNotesToStorage = (notes: NoteContent[]) => {
		let jsonToStore = JSON.stringify(notes);
		try {
			localStorage.setItem(this.noteStorage, jsonToStore);
		} catch (error) {
			toast("Couldn't add note! Please check your browser Settings!", {
				type: 'error',
			});
		}
	};
	getStoredNotes = () => {
		let storedJson = localStorage.getItem(this.noteStorage);
		let storedNotes: NoteContent[] = [];
		if (null === storedJson) {
			throw new Error('No Notes were found');
		} else {
			try {
				storedNotes = JSON.parse(storedJson);
			} catch (error) {
				throw new Error('No Notes were found');
			}
		}
		return storedNotes;
	};
	addNoteToStore = (noteToStore: NoteContent) => {
		let alreadyStoredNotes: NoteContent[] = [];
		try {
			alreadyStoredNotes = this.getStoredNotes();
		} catch (error) {
			toast('Adding first Note!', {
				type: 'info',
			});
			alreadyStoredNotes = [];
		}
		noteToStore.id = this.getRandomIdForNote(
			alreadyStoredNotes,
			noteToStore.id
		);
		alreadyStoredNotes.push(noteToStore);
		this.saveNotesToStorage(alreadyStoredNotes);
	};
	updateNoteInStore = (noteToupdate: NoteContent) => {
		let alreadyStoredNotes: NoteContent[] = [];
		try {
			alreadyStoredNotes = this.getStoredNotes();
		} catch (error) {
			alreadyStoredNotes = [];
			alreadyStoredNotes.push(noteToupdate);
		}
		let indexToUpdate = alreadyStoredNotes.findIndex(
			(note) => noteToupdate.id === note.id
		);
		if (-1 !== indexToUpdate) {
			alreadyStoredNotes[indexToUpdate].text = noteToupdate.text;
			this.saveNotesToStorage(alreadyStoredNotes);
		} else {
			this.addNoteToStore(noteToupdate);
		}
	};
	removeNoteFromStore = (id: number) => {
		let alreadyStoredNotes: NoteContent[] = [];
		try {
			alreadyStoredNotes = this.getStoredNotes();
			let indexToRemove = alreadyStoredNotes.findIndex(
				(note) => id === note.id
			);
			if (-1 !== indexToRemove) {
				alreadyStoredNotes.splice(indexToRemove, 1);
				this.saveNotesToStorage(alreadyStoredNotes);
			}
			toast('Successfully removed Note!', {
				type: 'success',
			});
		} catch (error) {
			alreadyStoredNotes = [];
		}
	};
}
